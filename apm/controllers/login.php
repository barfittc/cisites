<?php

/**
 * User Controller
 * This controller fully demonstrates the user class.
 *
 * @package User
 * @author Waldir Bertazzi Junior
 * @link http://waldir.org/
 **/
class Login extends MY_Controller {

	function __construct(){
		parent::__construct();

		// Load the Library
		//$this->load->library(array('user', 'user_manager'));
       //$this->load->helper('url');

	}

	function index()
	{
		// If user is already logged in, send it to private page
		$this->user->on_valid_session('/login/private_page');

		// Loads the login view
		//$this->load->view('login');
		$this->render(array(),'login');
	}

	function bad (){
		$this->json (array ('error' => 'not loged in', 'error_num' => 403));
	}

	function private_page(){
		// if user tries to direct access it will be sent to index
		$this->user->on_invalid_session('/login');

		// ... else he will view home
		//$this->load->view('home');
		//$this->render(array(),'admin');
		redirect (base_url ());
	}

	function validate()
	{
		// Receives the login data
		$login = $this->input->post('login');
		$password = $this->input->post('password');

		/*
		 * Validates the user input
		 * The user->login returns true on success or false on fail.
		 * It also creates the user session.
		*/
		if($this->user->login($login, $password)){
			// Success
			redirect (base_url ());
		} else {
			// Oh, holdon sir.
			redirect('/login');
		}
	}
	function password()
	{
		if (!$this->input->post('password'))
		{
			$this->json(array(
				'success'	=> false,
				'reason'	=> 'Password not entered',
			));
		}
		else
		{
			$this->user->update_pw($this->input->post('password'));
			$this->json(array(
				'success'	=> true
			));
		}
	}

	// Simple logout function
	function logout()
	{
		// Removes user session and redirects to login
		$this->user->destroy_user (base_url ());
	}
}
?>
