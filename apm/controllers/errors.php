<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Errors extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
        //$this->load->model('news_model', 'news');
	}
	
	public function _404()
	{
		$this->render(array('title'=>'error 404'),'error','404');
	}
}