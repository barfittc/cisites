<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Adminpages extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
       //$this->load->model('news_model', 'news');
		$this->user->on_invalid_session ('/login/bad');
	}

	public function index()
	{
	}
	public function add ()
	{

	}

	public function save ()
	{
		$page = $this->page ();

		foreach ($this->data as $key => $value)
			$page->$key = $value;

		$page->update ();

		$this->json (array (
				
		));
	}
	public function edit ()
	{

	}
	public function delete ()
	{

	}
}