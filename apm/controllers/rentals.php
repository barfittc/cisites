<?php if (!defined ('BASEPATH')) exit ('No direct script access allowed');

class Rentals extends MY_Controller
{
	public function __construct ()
		{ parent::__construct (); }
	private function showRentailsHome ($page, $ids)
		{ $this->render (array ('page' => $page, 'id' => $ids), 'rentals'); }
	public function index ()
		{ $this->showRentailsHome ('list', ''); }
	public function view ($id = '')
		{ $this->showRentailsHome ('view', $id); }
	public function find ($id = '')
		{ $this->showRentailsHome ('list', $id); }

	// This is where the action is.
	public function
		viewJSON			()
						{
							$res = $this->rental ()->toJSON ();

							if ($res ['VISIBLE'])
							{
								$this->json ($res);
							}
							else
							{
								$this->json (array (
									 'text' => 'Unable to find ID (' . $this->data ['ID'] . '). The listing has been removed.'
									 , 'code' => 404
									 , 'isError' => true
								));
							}
						}
	public function
		rentalsJSON			()
						{
							// Set, and override filters
							$this->data ['VISIBLE'] = TRUE;
							$this->data ['limit'] = 5;

							if (!isset ($this->data ['offset']))
								$this->data ['offset'] = 0;

							$this->json (array (
								 'data' => $this->rentals->getRentalsLIst ($this->data, true)
								 , 'count' => $this->rentals->getRentalsLIstCount ($this->data)
							));
						}
}