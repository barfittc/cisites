<?php if ( ! defined('BASEPATH')) exit ('No direct script access allowed');

class Adminrentals extends MY_Controller {

	public function
		__construct			()
						{
							parent::__construct ();
							$this->user->on_invalid_session ('/login/bad');
							$this->data = $this->input->post ();
						}
	public function
		index				()
						{
							$this->glist();
						}
	public function
		glist				()
						{
							$this->json (array (
									'visible'	=> $this->rentals->getRentalsLIst (array ('VISIBLE' => TRUE, 'limit' => 9999, 'offset' => 0))
									, 'hidden' => $this->rentals->getRentalsLIst (array ('VISIBLE' => FALSE, 'limit' => 9999, 'offset' => 0))
							));
						}
	public function
		get					()
						{
							$res = $this->rental ()->toJSON ();
							$res['isNew'] = false;
							$this->json ($res);
						}
	public function
		create				()
						{
							$newR = new Rental_model ();
							$res = $newR->add ()->toJSON ();
							$res['isNew'] = true;
							$this->json ($res);
						}
	public function
		save				()
						{
							$rental = $this->rental ();
							$this->data ['general'] ['AVAILABLE_BY'] = strtotime ($this->data ['general'] ['AVAILABLE_BY']);

							foreach ($this->data ['general'] as $key => $value)
								$rental->$key = $this->matchType ($rental->$key, $value);


							foreach ($this->data ['UTILITIES'] as $key => $value)
								$rental->util [$key] = $this->matchType ($rental->util [$key], $value);

							foreach ($this->data ['ADDRESS'] as $key => $value)
								$rental->address [$key] = $this->matchType ($rental->address [$key], $value);

							$rental->update ();
							$this->glist ();
						}
	private function
		matchType			($old, $new)
						{
							$oldType = gettype ($old);
							$newType = gettype ($new);
							if ($oldType != $newType)
							{
								switch ($oldType)
								{
									case "boolean":
										return filter_var ($new, FILTER_VALIDATE_BOOLEAN);
									case "integer":
										return intval ($new);
									case "double": // (for historical reasons "double" is returned in case of a float, and not simply "float")
										return doubleval ($new);
								}
							}
							return $new;
						}
	public function
		toggle				()
						{
							$rental = $this->rental ();
							$rental->VISIBLE = !$rental->VISIBLE;
							$rental->update ();
							$this->glist ();
						}
	public function
		delete				()
						{
							$rental = $this->rental ();
							$urls = $rental->remove ();
							foreach ($urls as $url)
								$this->removeFile ($url);
							$this->glist ();
						}
	public function
		thumbnail			()
						{
							$rental = $this->rental ();
							$res = $this->uploadFiles (true);
							$this->removeFile ($rental->thumnail ['LINK']);
							$rental->updateThumbnail ($res ['url']);
							$this->json (array (
								 'message' => $res ['message']
								, 'LINK' => $res ['url']
							));
						}
	public function
		addSlideshow			()
						{
							$rental = $this->rental ();
							//print_r ($rental);
							$res = $this->uploadFiles ();
							//print_r ($res);
							$img = $rental->addImage (array (
								 'LINK' => $res ['url']
								, 'CAPTION' => $this->data ['CAPTION']
							));

							$this->json (array (
								 'message' => $res['message']
								, 'LINK' => $img ['LINK']
								, 'CAPTION' => $img ['CAPTION']
								, 'ID' => $img ['ID']
								, 'data' => $this->data
							));
						}
	public function
		delSlideshow			()
						{
							$rental = $this->rental ();
							$img = $rental->removeImage ((int) $this->data ['img']);
							if (!($img === null))
							{
								$this->removeFile ($img ['LINK']);
								$this->json (array (
									 'success' => true
									, 'ID' => $img ['ID']
								));
							}
							else
							{
								$this->json (array (
									 'success' => false
									, 'ID' => $img ['ID']
								));
							}
						}
	public function
		updateRoom			()
						{
							$rental = $this->rental ();
							$res = array ();
							if ($this->data ['room'] ['ID'] === 'new')
								$res = $rental->addRoom ($this->data ['room']);

							else
								$res = $rental->updateRoom ($this->data ['room']);

							$this->json ($res);
						}
	public function
		delRoom				()
						{
							$rental = $this->rental ();
							$res = $rental->removeRoom ((int) $this->data ['room']);

							if ($res)
								$this->json (array (
									 'success' => true
									, 'ID' => (int) $this->data ['room']
								));

							else
								$this->json (array (
									 'success' => false
									, 'ID' => (int) $this->data ['room']
								));
						}
}