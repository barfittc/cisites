$(document).ready (function () {
	apm.rentals = {
		title: window.document.title
		, safetoAdd: true
		, url: {
			list: url + "index.php?/rentals/rentalsJSON"
			, view: url + "index.php?/rentals/viewJSON"
			, rental: url + "rentals/view/"
			, find: url + "rentals/find/"
			, galleria: url + 'js/galleria/themes/classic/galleria.classic.min.js'
		}
		, container: {
			listing: $("#rentalsResults")
			, logo: $("#logoContainer")
			, slideshow: $("#slideshowContainer")
			, content: $("#rightContent")
			, message: {
				text: ''
				, number: -1
				, isError: false
			}
		}
		, template: {
			result: $("#resultTemplate")
			, view: $("#viewTemplate")
			, search: $("#searchTemplate")
			, mapInfo: $("#mapInfoTemplate")
			, message: $("#messageTemplate")
			, searchResults: $("#searchResultsTemplate")
			, searchUtility: $("#searchUtilityTemplate")
			, image: $("#imageTemplate")
			, room: $("#roomTemplate")
			, utility: $("#utilityTemplate")
		}
		, current: 'none'
		, postBasic: function (url, data, callback)
						{
							$.ajaxSetup({ cache: false });
							$.post (url, data).done (function (res)
							{
								console.log (url, data, res);
								callback (res);
							});
						}
		, setURL: function (p, id, u, title)
						{
							if (!apm.rentals.safetoAdd)
								return;
							if (history.pushState)
								history.pushState ({
									'page': p
									, 'pageId': id
								}, title, u);
							window.document.title = title;
							pageId = id;
							page = p;
							apm.rentals.safetoAdd = true;
							console.log (p, id);
						}
		, search: {
			init: function ()
					{
						apm.map.rentals.clearMarkers ();
						if (apm.rentals.current === 'list')
							return;
						apm.rentals.current = 'list';
						$("#searchContainer").html (template.toHTML (apm.rentals.template.search));
						apm.rentals.container.content.html (template.toHTML (apm.rentals.template.searchResults));
						apm.rentals.container.listing = $("#rentalsResults");

						if (apm.rentals.container.message.code > -1)
							$("#messageContainer")
								.html (template.toHTML (apm.rentals.template.message, apm.rentals.container.message))
								.css ('display', 'block')
							;
						else
							$("#messageContainer").css ('display', 'none');
						apm.rentals.container.message = {
							text: ''
							, code: -1
							, isError: false
						};

						$('#filterSearchToggle')
							.removeProp ('checked')
							.attr ('checked', apm.rentals.search.filter.filter)
							.button ()
							.click (function (event)
							 {
								 $("#filterControls").toggle ('blind', { }, 500);
								 apm.rentals.search.filter.filter = !apm.rentals.search.filter.filter;
							 });
						if (apm.rentals.search.filter.filter)
							$("#filterControls").show ();
						else
							$("#filterControls").hide ();


						$('#pagePrev')
								  .button ({ icons: {  primary: "ui-icon-carat-1-w" } })
								  .click (function (event)
									{
										apm.rentals.search.filter.offset = apm.rentals.search.filter.offset - apm.rentals.search.offsetStep;
										apm.rentals.search.get ();
									});
						$('#pageNext')
								  .button ({ icons: {  secondary: "ui-icon-carat-1-e"  } })
								  .click (function (event)
									{
										apm.rentals.search.filter.offset = apm.rentals.search.filter.offset + apm.rentals.search.offsetStep;
										apm.rentals.search.get ();
									});
						apm.rentals.search.ui = {
							BEDROOMScount: $("#BEDROOMScount")
							, BATHROOMScount: $("#BATHROOMScount")
							, MIN_PRICE: $("#MIN_PRICE")
							, MAX_PRICE: $("#MAX_PRICE")
							, TYPE: $("#TYPE")
							, TYPECOMBO: $('#TYPE').siblings ('.ui-combobox, .custom-combobox').find ('.ui-autocomplete-input')
							, PRICE: $("#PRICE")
							, BEDROOMS: $("#BEDROOMS")
							, BATHROOMS: $("#BATHROOMS")
						};
						if (apm.rentals.search.filter ['TYPE'])
							apm.rentals.search.ui.TYPE.val (apm.rentals.search.filter ['TYPE']);
						apm.rentals.search.ui.TYPE.combobox ({ select: function (event, ui) {
							if (ui.item.value == 'Any')
								delete apm.rentals.search.filter ['TYPE'];
							else
								apm.rentals.search.filter ['TYPE'] = ui.item.value;
							apm.rentals.search.get ();
						} });

						apm.rentals.search.ui.BEDROOMS.slider ({
							range: "max"
							, min: 1
							, max: 10
							, value: apm.rentals.search.filter.countBed
							, stop: update
							, slide: function (event, ui) {
								apm.rentals.search.filter.countBed = ui.value;
								apm.rentals.search.labels ();
							}
						});

						apm.rentals.search.ui.BATHROOMS.slider ({
							range: "max"
							, min: 1
							, max: 5
							, value: apm.rentals.search.filter.countBath
							, stop: update
							, slide: function (event, ui) {
								apm.rentals.search.filter.countBath = ui.value;
								apm.rentals.search.labels ();
							}
						});

						apm.rentals.search.ui.PRICE.slider ({
							range: true
							, min: 0
							, max: 5000
							, step: 100
							, values: [
								apm.rentals.search.filter.PRICE_G
								, apm.rentals.search.filter.PRICE_L
							]
							, stop: update
							, slide: function (event, ui) {
								apm.rentals.search.filter.PRICE_G = ui.values [0];
								apm.rentals.search.filter.PRICE_L = ui.values [1];
								apm.rentals.search.labels ();
							}
						});

						function update (e, u) {
							apm.rentals.search.get ();
						}

					}
			, ui : {
				BEDROOMScount: null
				, BATHROOMScount: null
				, MIN_PRICE: null
				, MAX_PRICE: null
				, TYPE: null
				, PRICE: null
				, BEDROOMS: null
				, BATHROOMS: null
			}
			, labels: function ()
					{
						apm.rentals.search.ui.BEDROOMScount.html (apm.rentals.search.filter.countBed);
						apm.rentals.search.ui.BATHROOMScount.html (apm.rentals.search.filter.countBath);
						apm.rentals.search.ui.MIN_PRICE.html (apm.rentals.search.filter.PRICE_G);
						apm.rentals.search.ui.MAX_PRICE.html (apm.rentals.search.filter.PRICE_L);
						var t = apm.rentals.search.filter ['TYPE'] ? apm.rentals.search.filter ['TYPE'] : 'Any';
						if (t)
						{
							apm.rentals.search.ui.TYPE.val (t);
							apm.rentals.search.ui.TYPECOMBO.val (t);
						}

						apm.rentals.search.ui.PRICE.slider ('values', 0, apm.rentals.search.filter.PRICE_G);
						apm.rentals.search.ui.PRICE.slider ('values', 1, apm.rentals.search.filter.PRICE_L);
						apm.rentals.search.ui.BATHROOMS.slider ('value', apm.rentals.search.filter.countBath);
						apm.rentals.search.ui.BEDROOMS.slider ('value', apm.rentals.search.filter.countBed);
					}
			, get: function ()
					{
						apm.rentals.container.logo.show ();
						apm.rentals.container.slideshow.html ('');
						apm.rentals.container.slideshow.hide ();
						apm.rentals.search.init ();
						apm.rentals.search.labels ();
						apm.rentals.setURL ('list', apm.rentals.search.getFilterURL ()
								, apm.rentals.url.find + apm.rentals.search.getFilterURL ()
								, apm.rentals.title
						);

						apm.rentals.safetoAdd = true;
						apm.rentals.container.listing.html (template.toHTML (template.ajaxLoading));
						apm.rentals.postBasic (apm.rentals.url.list
							, apm.rentals.search.filter
							, apm.rentals.search.refresh
						);
					}
			, filter: {
			}
			, defaultFilter: {
				offset: 0
				, countBed: 0
				, countBath: 0
				, PRICE_G: 0
				, PRICE_L: 5000
				, filter: true
			}
			, count: 0
			, offsetStep: 5
			, getFilterURL: function ()
					{
						return encodeURIComponent (JSON.stringify (apm.rentals.search.filter));
					}
			, filterFromURL: function (urlFilter)
					{
						if (!(urlFilter == ''))
						{
							try {
								apm.rentals.search.filter = JSON.parse (decodeURIComponent (urlFilter));
							} catch (e) {
								console.error ('Unable to load filter from URL', e, urlFilter);
							}
						}
						else
							apm.rentals.search.filter = JSON.parse (JSON.stringify (apm.rentals.search.defaultFilter));
					}
			, current: [

			]
			, refresh: function (dataArray)
					{
						apm.rentals.container.listing.html ('');

						for (index = 0; index < apm.rentals.search.current.length; ++index)
							apm.rentals.search.remove (apm.rentals.search.current [index]);

						apm.rentals.search.current = dataArray.data;

						for (index = 0; index < dataArray.data.length; ++index) {
							if (index == dataArray.data.length - 1)
								dataArray.data [index] ['isLast'] = true;
							apm.rentals.search.add (dataArray.data [index], index === dataArray.data.length - 1);
						}

						apm.rentals.search.count = dataArray.count;
						$('#resShowing').html ('' + dataArray.data.length);
						$('#resCount').html ('' + apm.rentals.search.count);
						$('#pagePrev').button (apm.rentals.search.filter.offset > 0 ? 'enable' : 'disable');
						$('#pageNext').button ((apm.rentals.search.filter.offset + apm.rentals.search.offsetStep) < apm.rentals.search.count ? 'enable' : 'disable');
					}
			, add: function (data, isLast)
					{
						if (!isLast)
							data ['EXTRA_CLASS'] = 'border-bottom-active';
						data ['THUMBNAIL_URL'] = data ['THUMBNAIL'] ['LINK'];
						data ['URL'] = apm.rentals.url.rental + data ['ID'];
						data ['map'] = apm.map.rentals.createInfoBox (data ['ID']
							, data ['ADDRESS'] ['HOUSE_NUMBER'] + ' ' + data ['ADDRESS'] ['STREET']
							, template.toHTML (apm.rentals.template.mapInfo
								, {
									DESCRPITION: data ['DESCRPITION']
									, THUMBNAIL_URL: data ['THUMBNAIL_URL']
									, URL: data ['URL']
									, ID: data ['ID']
									, HOUSE_NUMBER: data ['ADDRESS'] ['HOUSE_NUMBER']
									, STREET: data ['ADDRESS'] ['STREET']
								}
							)
							, data ['ADDRESS'] ['Lat']
							, data ['ADDRESS'] ['Lng']
							, function ()
							{
								$('#mapInfoTitle_' + data ['ID'] + ', #mapInfoImg_' + data ['ID'])
									.click (resClick)
								;
							}
						);

						apm.rentals.container.listing.append (template.toHTML (apm.rentals.template.result, data));

						$('#marker_' + data ['ID']).click (data ['map'] ['open']);

						var zoom_slideshow = $('#zoom_slideshow_' + data ['ID'])
							.click (function (event) {
								stopEvent (event);
								console.log ('start Slideshow on', data);
								var Name = 'slideshow_' + data ['ID'] + '_' + rnd () + 'Container';
								dialog.create (
									'<div id="'+Name + '"></div>'
									, {
										title: 'Slideshow'
										, autoOpen: true
										, height: 365
										, width: 335
										, modal: true
										, resizable: false
										, buttons: {}
										, create: function (event, ui) {
											apm.rentals.Galleria.createIn ($('#' + Name), data ['IMAGES']);
										}
									}
								);
							})
						;
						$('#thumbnail_' + data ['ID'])
							.mouseover (function (event) {
								zoom_slideshow.css ('display', 'block');
							})
							.mouseout (function (event) {
								zoom_slideshow.css ('display', 'none');
							})
						;
						$('#apply_' + data ['ID'])
							.click (function (event) {
							})
						;
						$('#resault_' + data ['ID'])
							.click (resClick)
						;
						var amenities = $('#amenitiesContainer_' + data ['ID']);
						for (var prop in data ['UTILITIES'])
							if (data ['UTILITIES'].hasOwnProperty (prop)
							&& data ['UTILITIES'] [prop])
								amenities.append (template.toHTML (apm.rentals.template.searchUtility, {
									'TEXT' : getUtilText (prop)
									, 'LINK' : "images/UTILITIES/" + prop + ".png"
								}));

						function resClick (event) {
							stopEvent (event);
							apm.rentals.rental.show (data);
						}
						var amenitiesContainer = $("#amenitiesContainer_" + data ['ID']);
						//console.log ('added', data);
						function stopEvent (event) {
							if (event && event.preventDefault)
								event.preventDefault ();
							if (event && event.stopPropagation)
								event.stopPropagation ();
						}
					}
			, remove: function (data)
					{

					}
		}
		, Galleria: {
			init: function (id, opt)
			{
				if (!apm.rentals.Galleria.isInit)
				{
					apm.rentals.Galleria.isInit = true;
					// http://galleria.io/themes/azur/
					Galleria.loadTheme (apm.rentals.url.galleria);
					Galleria.configure ({ width: 314 , height: 314 });
					Galleria.ready (function ()
					{
						var gallery = this;
						gallery.addElement ('fscr');
						gallery.appendChild ('stage', 'fscr');
						this.$('fscr').click (function ()
						{
							gallery.toggleFullscreen ();
						});
					});
				}
				Galleria.run (id);
			}, isInit:false
			, createIn: function (slideShowCont, array)
			{
				for (index = 0; index < array.length; ++index)
					slideShowCont.append (template.toHTML (apm.rentals.template.image, array [index]));
				apm.rentals.Galleria.init (slideShowCont);
			}
		}
		, rental: {
			fromSearch: false
			, init: function ()
					{
						apm.map.rentals.clearMarkers ();
						if (apm.rentals.current === 'view')
							return;
						apm.rentals.current = 'view';
						apm.rentals.container.content.html (template.toHTML (template.ajaxLoading));
					}
			, get: function (id)
					{
						apm.rentals.rental.init ();
						apm.rentals.postBasic (apm.rentals.url.view
							, {
								ID: id
							}
							, function (data)
							{
								if (data ['code'] && data ['isError'])
								{
									apm.rentals.container.message = data;
									apm.rentals.search.get ();
								}
								else
								{
									apm.rentals.rental.show (data);
								}
							}
						);
					}
			, show: function (data)
					{
						apm.rentals.rental.init ();
						apm.rentals.container.logo.hide ();
						apm.rentals.container.slideshow.show ();
						apm.rentals.setURL ('view', data.ID, apm.rentals.url.rental + data.ID
							, "Viewing Rental #" + data.ID + " - " + apm.rentals.title
						);

						data ['SLIIDESHOWID'] = 'slideshow_' + data ['ID'] + '_' + rnd ();
						apm.rentals.container.slideshow.html ('<div id="'+data ['SLIIDESHOWID']+'Container"></div>');
						//console.log ('show rental ' + data);
						apm.rentals.container.content.html (template.toHTML (apm.rentals.template.view, data));

						$('#apply')
							.button ({ icons: { primary: "ui-icon-document-b" } })
							.click (function (event) {
								window.open ("https://docs.google.com/forms/d/1sHC6bvKbhefCpxvaLHMno4C-NAeJrJawoDCa3IuGZgE/viewform?entry.1632457232=" + data ['ID']);
						})
						;
						var bFunc = function (event) {
							//apm.rentals.search.filterFromURL ('');
							apm.rentals.search.get ();
						};
						$('#back')
							.button ({ icons: { primary: "ui-icon-arrowthick-1-w" } })
							.click (bFunc)
						;
						$('#backBot')
							.button ({ icons: { primary: "ui-icon-arrowthick-1-w" } })
							.click (bFunc)
						;
						//$('.at15t_compact')
						//	.button ({ icons: { primary: "ui-icon-mail-closed" } })
						//	.click (function (event) {
						//	})
						//;
						$('#shareContainer').append ('<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-52da56815d4d3ebf" />');

						var utilitiesContainer = $('#utilitiesContainer');
						delete data.UTILITIES.GAS;
						delete data.UTILITIES.GAS_AVAILABLE;
						$.each (data ['UTILITIES'], function (index, value)
						{
							if (value)
								utilitiesContainer.append (template.toHTML (apm.rentals.template.utility, {
									LINK: "images/UTILITIES/" + index + ".png"
									, TEXT: getUtilText (index)
								}));
						});
						apm.rentals.Galleria.createIn ($('#' + data ['SLIIDESHOWID'] + "Container"), data ['IMAGES']);

						var roomsContainer = $('#roomsContainer');
						for (index = 0; index < data ['ROOMS'].length; ++index)
							roomsContainer.append (template.toHTML (apm.rentals.template.room, data ['ROOMS'] [index]));

						data ['map'] = apm.map.rentals.createInfoBox (data ['ID']
							, data ['ADDRESS'] ['HOUSE_NUMBER'] + ' ' + data ['ADDRESS'] ['STREET']
							, null
							, data ['ADDRESS'] ['Lat']
							, data ['ADDRESS'] ['Lng']
						);
						apm.map.maps.main.setCenter (data ['map'].marker.getPosition ());
					}
		}
	};
	function load (p, pid)
	{
		console.log ('load', p, pid);
		if (p === 'list')
		{
			apm.rentals.search.filterFromURL (pid);
			apm.rentals.search.get ();
		}
		else
			apm.rentals.rental.get (pid);
	}
	var dPage = page, dPageId = pageId;

	if (dPage === 'list' && !(dPageId == ''))
	{
		try {
			apm.rentals.search.defaultFilter = JSON.parse (decodeURIComponent (dPageId));
		} catch (e) {
			console.error ('Unable to load filter from URL', e, dPageId);
		}
	} else
		apm.rentals.search.filterFromURL ('');

	window.onpopstate = function (event) {
		console.log (event);

		apm.rentals.safetoAdd = false;
		if (event.state)
			load (event.state.page, event.state.pageId);

		else
			load (dPage, dPageId);

	};
	load (dPage, dPageId);
});
function getUtilText (name)
{
	if (name == "GAS")
		return "Gas is included";
	if (name == "HEAT")
		return "Heat is included";
	if (name == "INTERNET")
		return "Internet is included";
	if (name == "POWER")
		return "Power is included";
	if (name == "WATER")
		return "Water is included";

	if (name == "GAS_AVAILABLE")
		return "Gas is available";
	if (name == "LAUNDRY")
		return "Laundry is available";
	if (name == "PARKING")
		return "Parking is available";

	if (name == "PETS")
		return "Pets are allowed";
	if (name == "SMOKING")
		return "Smoking is allowed";

	return "";
}