<div id="slideshow" style="display:none;"></div>

<table width="100%" cellspacing="0" cellpadding="25">
	<tr>
		<td id="body-left" valign="top">
			<div class="body-square pageBanner no-pad tab" id="logoContainer">
				<img src="<?php echo $bannerSRC ?>" />
			</div>
			<div class="body-square pageBanner no-pad" id="slideshowContainer" style="display: none;"></div>
			<div id="searchContainer">
			</div>
			<?php $this->load->view('map.view.php');?>
		</td>
		<td width="100%"></td>
		<td id="body-right" valign="top">
			<div id="rightContent"></div>
		</td>
	</tr>
</table>
<script type="text/javascript">
	var page = '<?php echo $page; ?>';
	var pageId = '<?php echo $id; ?>';
</script>

<?php if ($admin): ?>
	<script src="<?php echo base_url('/'); ?>js/wysiwyg/parser_rules/simple.js"></script>
	<script src="<?php echo base_url('/'); ?>js/wysiwyg/dist/wysihtml5-0.3.0.js"></script>
<?php endif; ?>
