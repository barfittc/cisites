var dialog;
var template;
var url = "<?php echo base_url(''); ?>" ;
var rnd = function (max)
{
	if (!max) max = 10;
	return Math.floor ((Math.random () * max) + 1);
};
var wrapper, body = document.body, html = document.documentElement;
var addEvent = function (elem, type, eventHandle) {
	if (elem === null || elem === undefined)
		return;
	if (elem.addEventListener)
		elem.addEventListener (type, eventHandle, false);
	else if (elem.attachEvent)
		elem.attachEvent ("on" + type, eventHandle);
	else
		elem ["on" + type] = eventHandle;
};
var myResize = function (e) {
	wrapper.height (Math.max (
			body.scrollHeight
			, body.offsetHeight
			, html.clientHeight
			, html.scrollHeight
			, html.offsetHeight
	));
};
$(document).ready(function () {
	wrapper = $("#wrapper");
	//addEvent (window, "resize", myResize);
	window.setInterval (myResize, 500);
	//myResize ();
	$.ajaxSetup({ cache: false });
	template =
		{
			toHTML: function (tmp, data) { return tmp.tmpl (data || [ { } ]) [0].outerHTML; }
			, toObject: function (tmp, data) { return $(template.toHTML(tmp, data)); }

			, ajaxLoading: $("#ajaxLoading")
			, ajaxFailed: $("#ajaxFailed")
			, ajaxSuccess: $("#ajaxSuccess")
		};
	dialog = {
		container: $("#dialogs")
		, submitForm: function (diag, form, url) {
			var butnsBkup = diag.dialog ("option", "buttons");
			var htmlBkup = diag.html ();

			diag.html (template.toHTML (template.ajaxLoading));
			diag.dialog ("option", {
				buttons: null
			});

			$.post (
				url
				, form.serialize ()
				, function (data) {
					if (data.success) {
						success ();
					} else {
						failed (data.reason);
					}
				}
				, 'json'
			).fail(function () {
				failed("HTTP REQUEST ERROR.");
			});

			function success () {
				diag.html(template.toHTML(template.ajaxSuccess));
				diag.dialog("option", {
					buttons: {
						Ok: function () {
							dialog.destroy(diag);
						}
					}
				});
			}
			function failed (failedReason) {
				diag.html(template.toHTML(template.ajaxFailed, { reason: failedReason }));
				diag.dialog("option", {
					buttons: {
						Ok: function () {
							dialog.destroy(diag);
						}
					}
				});
			}
		}
		, destroy: function (diag) {
			var id = diag.prop('id');
			diag.dialog("destroy");
			(elem = document.getElementById(id)).parentNode.removeChild(elem);
		}
		, close: function () {
			dialog.destroy ($(this));
		}
		, create: function (content, options, oID) {
			var id = oID || String.fromCharCode(65 + Math.floor(Math.random() * 26)) + Date.now();
			dialog.container.append ('<div id="' + id + '" title="">' + content + '</div>');
			return $("#" + id).dialog(options);
		}
		, show: function (diag) {
			diag.dialog('open');
		}
	};
});