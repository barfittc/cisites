<script id="adminErrorTemplate" type="text/x-jquery-tmpl">
	<div class="ui-state-error ui-corner-all" style="padding: 0 .7em;">
		<p>
			<span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
			<strong>Error ${code}:</strong><br />
			An error occurred.<br />
			'${text}'
		</p>
	</div>
</script>
<script id="adminListRentalsTemplate" type="text/x-jquery-tmpl">
	<div>
		<button id="del${ID}">Delete</button>
		<button id="toggle${ID}">{%if VISIBLE%}Hide{%else%}Show{%/if%}</button>
		<button id="edit${ID}" class="admEdit ui-button ui-widget {%if VISIBLE%}ui-widget-content ui-state-default{%else%}ui-widget-content{%/if%} ui-corner-all ui-button-text-icon-primary" style="width:85%;">
			<span class="ui-button-icon-primary ui-icon ui-icon-pencil"></span>
			<span class="ui-button-text" style="text-align: left;">#${ID} - ${TITLE}</span>
		</button>
	</div>
</script>
<script id="adminRoomTemplate" type="text/x-jquery-tmpl">
	<table width="100%" id="room_${ID}">
		<tr>
			<td width="50%">
				Name:
				<input id="roomName_${ID}" type="text" class="ui-corner-all ui-widget-content" size="15" value="${ROOM_NAME}" />
			</td>
			<td>
				<label for="roomType_${ID}">Type:</label>
				<select id="roomType_${ID}">
					<option value="Bedroom">Bedroom</option>
					<option value="Bathroom">Bathroom</option>
					<option value="Kitchen">Kitchen</option>
					<option value="Living Area">Living Area</option
				</select>
			</td>
		</tr>
		<tr>
			<td>
				<button id="roomDelete_${ID}">Delete</button>
				<button id="roomSave_${ID}">Save</button>
			</td>
			<td>
				<input id="roomSizeX_${ID}" type="hidden" class="ui-corner-all ui-widget-content" size="4" value="1" value2="${DIMENSION_1}" />
				
				<input id="roomSizeY_${ID}" type="hidden" class="ui-corner-all ui-widget-content" size="4" value="1" value2="${DIMENSION_2}" />
			</td>
		</tr>
	</table>
</script>
<script id="adminSlideshowTemplate" type="text/x-jquery-tmpl">
	<table width="100%" id="slideshow_${ID}">
		<tr>
			<td>
				<img src="<?php echo base_url('') ?>${LINK}" alt="Missing Thumbnail, Please Upload Image." class="adminImage" width="105" height="105"  />
			</td>
			<td width="100%" valign="top">
				<button id="slideshowDelete_${ID}">Remove Image</button>
				<br />
				Caption: <br />${CAPTION}
			</td>
		</tr>
	</table>
</script>
<script id="adminUtilitiesTemplate" type="text/x-jquery-tmpl"><div>
	<input type="checkbox" id="utilitiesParking" name="PARKING" {%if PARKING%} checked="checked"{%/if%}><label for="utilitiesParking">Parking included</label><br />
	<input type="checkbox" id="utilitiesPower" name="POWER"{%if POWER %} checked="checked"{%/if%}><label for="utilitiesPower">Power included</label><br />
	<input type="checkbox" id="utilitiesHeat" name="HEAT"{%if HEAT %} checked="checked"{%/if%}><label for="utilitiesHeat">Heat included</label><br />
	<input type="checkbox" id="utilitiesWater" name="WATER"{%if WATER %} checked="checked"{%/if%}><label for="utilitiesWater">Water included</label><br />
	<input type="checkbox" id="utilitiesLaundry" name="LAUNDRY"{%if LAUNDRY %} checked="checked"{%/if%}><label for="utilitiesLaundry">Laundry available at location</label><br />
	<input type="checkbox" id="utilitiesInternet" name="INTERNET"{%if INTERNET %} checked="checked"{%/if%}><label for="utilitiesInternet">Internet included</label><br />
	<input type="checkbox" id="utilitiesSmoking" name="SMOKING"{%if SMOKING %} checked="checked"{%/if%}><label for="utilitiesSmoking">Smoking allowed</label><br />
	<input type="checkbox" id="utilitiesPets" name="PETS"{%if PETS %} checked="checked"{%/if%}><label for="utilitiesPets">Pets allowed</label></div>
</script>
<script id="adminAddressTemplate" type="text/x-jquery-tmpl">
	<table width="100%">
		<tr>
			<td colspan="2">
				<input id="addressLat" name="Lat" type="hidden" value="${Lat}" />
				<input id="addressLng" name="Lng" type="hidden" value="${Lng}" />
				<input id="addressNumber" name="HOUSE_NUMBER" type="text" class="ui-corner-all ui-widget-content" size="10" placeholder="Number" value="${HOUSE_NUMBER}" />
				<input id="addressName" name="STREET_NAME" type="text" class="ui-corner-all ui-widget-content" size="50" placeholder="Street Name (Victoria St)" value="${STREET}" />
				<input id="addressCity" name="CITY" type="text" class="ui-corner-all ui-widget-content" size="10" placeholder="City" value="${CITY}" />
			</td>
		</tr>
		<tr>
			<td valign="top" width="160" nowrap>
				<input id="PROVINCE" name="PROVINCE" type="hidden" value="${PROVINCE}" />
				<button id="findAddress">Find on Map</button>
			</td>
			<td width="100%">
				<div id="addressMap"></div>
			</td>
		</tr>
	</table>
</script>
<script id="adminThumbnailTemplate" type="text/x-jquery-tmpl">
	<div>
		Thumbnail:<br />
		<div style="width:105px;height:105px;" class="adminImage"><img src="<?php echo base_url('') ?>${LINK}" width="105" height="105" id="thumbnailImg" alt="Missing Thumbnail, Please upload one" /></div><br />
		<button id="picturesChangeThumbnail">Change</button>
		<input type="file" name="picturesChangeThumbnailFile" id="picturesChangeThumbnailFile" /><br />
		<div id="details"></div>
		<div id="response"></div>
	</div>
</script>
<script id="adminRentalTemplate" type="text/x-jquery-tmpl">
	<div id="editRentalAccordion">
		<input id="ID" name="ID" type="hidden" value="${ID}" />
		<h3>General</h3>
		<div>
			<form id="editRentalFormGeneral">
				<table width="100%">
					<tr>
						<td width="50%">
							<label for="generalRentalType">Type:</label>
							<select id="generalRentalType" name="TYPE">
								<option value="Apartment">Apartment</option>
								<option value="Commercial">Commercial</option>
								<option value="Faculty">Faculty</option>
								<option value="House">House</option>
								<option value="Room">Room</option>
								<option value="Shared">Shared</option>
							</select>
						</td>
						<td>
							<label for="generalLeaseLength">Lease Length:</label>
							<input id="generalLeaseLength" name="LEASE_LENGTH" value="${LEASE_LENGTH}">
							 Months
						</td>
					</tr>
					<tr>
						<td>
							Price $
							<input id="generalPrice" name="PRICE" type="text" class="ui-corner-all ui-widget-content" size="4" placeholder="0000" value="${PRICE}" />
							 a month
						</td>
						<td rowspan="3" valign="top">Available by:<input type="text" id="generalAvailable" name="AVAILABLE_BY" value="${AVAILABLE_BY}"></td>
					</tr>
					<tr>
						<td colspan="2">
							<label for="generalDescription">Description:</label><br />
							<textarea id="generalDescription" name="DESCRPITION" class="ui-corner-all ui-widget-content" style="margin: 0px; height: 100px; width: 100%;">${DESCRPITION}</textarea>
						</td>
					</tr>
					<tr>
						<td>
							<div id="generalChecks">
								<input type="checkbox" id="generalVisible" name="VISIBLE"{%if VISIBLE%} checked="checked"{%/if%} /><label for="generalVisible">Visible</label>
								<input type="checkbox" id="generalBigAdvert" name="BIG_ADVERT"{%if BIG_ADVERT%} checked="checked"{%/if%} /><label for="generalBigAdvert">Big Advert</label>
							</div>
						</td>
					</tr>
				</table>
			</form>
		</div>
		<h3>Rooms</h3>
		<div>
			<div>
				<div id="admRoomsContainer"></div>
				<button id="roomsAdd">New Room</button>
			</div>
		</div>
		<h3 id="utilities">Utilities</h3>
		<div>
			<div id="admUtilitiesContainer"></div>
		</div>
		<h3>Address</h3>
		<div>
			<div id="admAddressContainer"></div>
		</div>
		<h3>Pictures</h3>
		<div>
			<table width="100%">
				<tr>
					<td valign="top">
						<div id="admThumbnailContainer"></div>
					</td>
					<td width="100%" valign="top">
						Slideshow:
						<table width="100%">
							<tr>
								<td>
									<div style="width:105px;height:105px;" class="adminImage"><img src="aoe.jpg" id="slideshowUploadImg" width="105" height="105" /></div>
								</td>
								<td width="100%" valign="top">
									Caption: <br /> <textarea id="slideShowCaption" rows="3" cols="37" class="ui-corner-all ui-widget-content"></textarea>
									<button id="picturesAddSlideShow">Pick file, and Save</button>
									<input type="file" name="picturesAddSlideShowFile" id="picturesAddSlideShowFile" />
								</td>
							</tr>
						</table>
						<div id="admSlideshowContainer"></div>
					</td>
				</tr>
			</table>
		</div>
	</div>
</script>
