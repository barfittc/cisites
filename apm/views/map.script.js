var apm;
$(document).ready(function () {
	apm =
	{
		map:
		{
			rentals:
				{
					markers: []
					, infoBoxes: []
					, clearMarkers: function () {
						for (index = 0; index < apm.map.rentals.markers.length; ++index)
						{
							apm.map.rentals.markers [index].setMap (null);
							apm.map.rentals.infoBoxes [index].setMap (null);
						}
						apm.map.rentals.infoBoxes = [];
						apm.map.rentals.markers = [];
					}
					, currentInfoWindow: false
					, createInfoBox: function (id, title, content, lat, lng, onOpen) {
						apm.map.rentals.infoBoxes.push (new google.maps.InfoWindow ({
							size: new google.maps.Size (200, 150)
							, content: content
						}));
						apm.map.rentals.markers.push (new google.maps.Marker (
						{
							position: new google.maps.LatLng (lat, lng)
							, title: title
							, icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=' + id + '|ff7c70|000000'
							, map: apm.map.maps.main
						}));
						var r = {
							marker: apm.map.rentals.markers [apm.map.rentals.markers.length - 1]
							, info: apm.map.rentals.infoBoxes [apm.map.rentals.infoBoxes.length - 1]
							, open: function (event) {
								if (event && event.stopPropagation)
									event.stopPropagation ();
								if (apm.map.rentals.currentInfoWindow && apm.map.rentals.currentInfoWindow.close)
									apm.map.rentals.currentInfoWindow.close ();

								if (content)
									r.info.open (apm.map.maps.main, r.marker);
								else
									apm.map.maps.main.setCenter (r.marker.getPosition ());
								apm.map.checkMoved ();
								apm.map.rentals.currentInfoWindow = r.info;
								
								if (onOpen)
									onOpen ();
							}
						};
						google.maps.event.addListener (r.marker, 'click', r.open);
						return r;
					}
				}
			, maps:
				{
					Options:
						{
							zoom: 13,
							center: new google.maps.LatLng(44.23142, -76.48101),
							mapTypeControl: false,
							navigationControl: true,
							streetViewControl: false,
							mapTypeId: google.maps.MapTypeId.ROADMAP,
							mapTypeControlOptions: { style: google.maps.MapTypeControlStyle.DROPDOWN_MENU }
						},
					main: {},
					markers: [],
					hidden: {}
				}
			, init: function () {
				apm.map.maps.main = new google.maps.Map(document.getElementById("map"), apm.map.maps.Options);
				google.maps.event.addListener(apm.map.maps.main, 'dragend', apm.map.refresh);

				apm.map.maps.hidden = new google.maps.Map(document.getElementById("mapHidden"), apm.map.maps.Options);
				apm.map.get.places('gas', "blue", true);// this is to prevent spaming of yellowpages.ca

				for (var i = 0; i < apm.map.places.toActivate.length; i++)
					$("#" + apm.map.places.toActivate[i].name).button().prop("checked", false).prop("indx", i).click(function () {
						apm.map.places.toActivate[$(this).prop('indx')].active = $(this).is(':checked');
						apm.map.refresh();
					});

			}
			, checkMoved: function () {
				setInterval (apm.map.refresh, 500);
			}
			, refresh: function () {
				var moved = !(apm.map.maps.Options.center == apm.map.maps.main.getCenter ());
				if (moved)
					apm.map.checkMoved ();
				for (var i = 0; i < apm.map.places.toActivate.length; i++) {
					var update = false;
					// this will always be updated, we moved and it's active, so refresh the map.
					if (moved && apm.map.places.toActivate[i].active)
						update = true;

						// we are active but map hasn't been moved
					else if (apm.map.places.toActivate[i].active) {

						// we need to check if it is not curently on the map.
						if (apm.map.places[apm.map.places.toActivate[i].name][0] == null || apm.map.places[apm.map.places.toActivate[i].name][0].getMap() == null)
							update = true;

						else if (apm.map.places[apm.map.places.toActivate[i].name][0].getMap() == apm.map.maps.main)
							continue;
					}

					if (update)
						apm.map.places[apm.map.places.toActivate[i].name] = apm.map.get.places(apm.map.places.toActivate[i].name, apm.map.places.toActivate[i].color);
					else
						apm.map.places.deActivate(apm.map.places[apm.map.places.toActivate[i].name]);
				}
			}
			, get:
				{
					marker: function (iconColor) //  ["red","blue","green","yellow","orange","purple","pink","ltblue"]
					{
						if ((typeof (iconColor) == "undefined") || (iconColor == null) || (iconColor == "")) {
							iconColor = "red";
						}
						if (!apm.map.maps.markers[iconColor]) {
							apm.map.maps.markers[iconColor] = new google.maps.MarkerImage(
								"http://www.google.com/intl/en_us/mapfiles/ms/micons/" + iconColor + "-dot.png",
								new google.maps.Size(32, 32),	// This marker is 32 pixels wide by 32 pixels tall.
								new google.maps.Point(0, 0),	// The origin for this image is 0,0.
								new google.maps.Point(16, 32)	// The anchor for this image is at 16,32.
							);
						}
						return apm.map.maps.markers[iconColor];
					},
					places: function (keyword, colour, hide) {
						var theArray = apm.map.places[keyword];
						var req = {
							location: apm.map.maps.Options.center = apm.map.maps.main.getCenter (),
							keyword: keyword,
							rankBy: google.maps.places.RankBy.DISTANCE
						};
						service = new google.maps.places.PlacesService (apm.map.maps.hidden);
						service.search(req, function (results, status) {
							if (status == google.maps.places.PlacesServiceStatus.OK) {
								for (var i = 0; i < results.length; i++) {
									var place = results [i];
									var marker = new google.maps.Marker (
									{
										position: new google.maps.LatLng(results[i].geometry.location.lat (), results[i].geometry.location.lng ()),
										icon: apm.map.get.marker (colour),
										title: results[i].name,
										map: apm.map.maps.hidden
									});
									if (!hide) {
										if (!(theArray[i] == null) && !(theArray[i].getMap () == null))
											theArray[i].setMap (null);

										theArray[i] = marker;
										theArray[i].setMap (apm.map.maps.main);
									}
									else {
										marker.setMap (null);
									}
								}
							}
						});
						return theArray;
					}
				}
			, places:
				{
					deActivate: function (theArray) {
						for (var x = 0; x < theArray.length; x++)
							if (!(theArray[x] == null))
								theArray[x].setMap (null);
					}
					, toActivate: [
							{ name: 'food', color: 'blue', active: false },
							{ name: 'coffee', color: 'orange', active: false },
							{ name: 'grocery', color: 'green', active: false },
							{ name: 'school', color: 'yellow', active: false },
							{ name: 'parks', color: 'ltblue', active: false },
							{ name: 'gas', color: 'purple', active: false }]
					, food: []
					, coffee: []
					, grocery: []
					, gas: []
					, parks: []
					, school: []
				}
		}
	};
	apm.map.init();
});