<div id="slideshow" style="display:none;"></div>

<table width="100%" cellspacing="0" cellpadding="25">
	<tr>
		<td id="body-left" valign="top">
			<div class="body-square pageBanner no-pad tab" id="pageBanner">
				<img src="<?php echo $bannerSRC ?>" />
			</div>
		</td>
		<td width="100%"></td>
		<td id="body-right" valign="top">
			<div class="body-square">
				<div class="title" id="pageTitle"><?php echo $page_title; ?></div>
				<div id="pageContent"><?php echo $content; ?></div>
			</div>
		</td>
	</tr>
</table>
	<?php if ($admin): ?>
		<script src="<?php echo base_url('/'); ?>js/aloha/lib/require.js"></script>
		<script src="<?php echo base_url('/'); ?>js/aloha/lib/aloha.js"
				  data-aloha-plugins="common/ui,
											 common/format,
											 common/table,
											 common/list,
											 common/link,
											 common/undo,
											 common/contenthandler,
											 common/paste,
											 common/characterpicker,
											 common/commands,
											 common/block,
											 common/image,
											 common/abbr,
											 common/horizontalruler,
											 common/align,
											 common/dom-to-xhtml,
											 extra/textcolor,
											 extra/formatlesspaste,
											 extra/hints,
											 extra/toc,
											 extra/headerids,
											 extra/listenforcer,
											 extra/metaview,
											 extra/numerated-headers,
											 extra/ribbon,
											 extra/wai-lang,
											 extra/flag-icons,
											 extra/linkbrowser,
											 extra/imagebrowser,
											 extra/cite"></script>
	<script type="text/javascript">
		var pageId = <?php echo $id ?>;
	</script>
	<?php endif; ?>
