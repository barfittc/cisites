
<script id="searchTemplate" type="text/x-jquery-tmpl">
	<div style="display:none;">
		<div id="pageation" class="body-square no-pad" style="min-height:40px;">
			<div class="title-no-pad">Narrow Results</div>
				<div style="text-align:center;">
				<div id="filterControls">
					<?php $this->load->view("rentals.filters.html"); ?>
				</div>
			</div>
			<div style="float:right; margin: 10px 10px 10px 10px; bottom:10px;"><button id="pageNext" class="yellowButtonUI">Next</button></div>
			<div style="margin: 10px 10px 10px 10px;"><button id="pagePrev" class="yellowButtonUI">Previous</button></div>
			<input type="checkbox" id="filterSearchToggle" class="yellowButtonUI" /><label for="filterSearchToggle" class="yellowButtonUI">Narrow Results</label>
			<div style="height:2px;"></div>
		</div>
	</div>
</script>
<script id="searchResultsTemplate" type="text/x-jquery-tmpl">
	<div>
		<div class="body-square" >
			<div class="title">Apartments for Rent in Kingston</div>
			Showing results <span id="resShowing">0</span> of <span id="resCount">0</span>
		</div>
		<div id="messageContainer"></div>
		<div class="body-square" style="display:none;">
			<div class="filterSearchToggle"></div>
			<p id="pageDescription">
				A short write up on our rentals.... A short write up on our rentals.... A short write up on our rentals....
			</p>
		</div>
		<div class="mainSpacer" style="display:none;"></div>
		<div class="border-all content">
			<div id="rentalsResults"class="body-square"></div>
		</div>
	</div>
</script>
<script id="messageTemplate" type="text/x-jquery-tmpl">
	<div>
		<div class="spacer"></div>
		<div class="ui-state-{%if isError%}error{%else%}highlight{%/if%} ui-corner-all">${text}</div>
		<div class="spacer"></div>
	</div>
</script>
<script id="resultTemplate" type="text/x-jquery-tmpl">
	<div class="body-square no-pad body-square-no-pad {%if BIG_ADVERT%}resault_big{%else%}resault_small{%/if%} content ${EXTRA_CLASS}" id="resault_${ID}" style="width:454px;{%if isLast%}margin-bottom:0px;{%/if%}">
		<div id="marker_${ID}" class="marker"><img width="21" height="34" src="http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=${ID}|ff7c70|000000" /></div>
			<div class="detailsNames">
			Type<br />
			Price<br />
			Bedrooms<br />
			Bathrooms{%if BIG_ADVERT%}<br />Amenities{%/if%}
			<div id="amenitiesContainer_${ID}" style="padding-top:5px; display:{%if BIG_ADVERT%}block{%else%}none{%/if%};"></div>
				</div>
			<div class="details">
				${TYPE}<br />
				$${PRICE}<br />
				${countBed}<br />
				${countBath}<br />
			</div>
			<div id="thumbnail_${ID}" class="thumbnail">
				<div id="zoom_slideshow_${ID}" class="zoom">
					 <img width="26" height="26" src="<?php echo base_url (); ?>images/clear.png" />
				 </div>
				 <img width="{%if BIG_ADVERT%}154{%else%}79{%/if%}" height="{%if BIG_ADVERT%}154{%else%}79{%/if%}" src="<?php echo base_url (); ?>${THUMBNAIL_URL}" />
			</div>
			<div class="links" style="display:none;">
				<div id="apply_${ID}">Apply Now!</div>
			</div>
		<div class="clear"></div>
	</div>
</script>
<script id="mapInfoTemplate" type="text/x-jquery-tmpl">
	<div style="width:200px; height: 100px; overflow: hidden;">
		<a id="mapInfoTitle_${ID}" href="${URL}" class="InfoTitle"><strong>${HOUSE_NUMBER} ${STREET}</strong></a><br />
		<a id="mapInfoImg_${ID}" href="${URL}" class="InfoTitle"><img width="74" height="74" src="<?php echo base_url (); ?>${THUMBNAIL_URL}" style="float:left; border:solid 5px;border-color:#FFF;" /></a>
		<span style="max-width:75px; color:#000000;">${DESCRPITION}</span>
	</div>
</script>
<script id="viewTemplate" type="text/x-jquery-tmpl">
	<div>
		<div class="body-square">
			<div class="title">
				Viewing Rental #${ID}
			</div>

		<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td>
					<div id="shareContainer"">
						<div id="shareContainer" style="float:right;">
							<div class="addthis_toolbox addthis_default_style addthis_32x32_style">
								<a class="addthis_button_compact">Share</a>
							</div>
							<button id="apply" class="yellowButtonUI">Apply Online</button>
						</div>
						<button id="back" class="yellowButtonUI">Go Back</button>
					</div>
				</td>
			</tr>
			<tr>
				<td><br />
					{%html DESCRPITION%}
				</td>
			</tr>
		</table>
		</div>

		<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td width="234" style="width:234px;">
					<div class="body-square no-pad body-square-no-pad" style="width:224px; padding: 5px 5px 5px 5px;">
						<strong>Price</strong>: $${PRICE}<br />
						<strong>Lease Length</strong>: ${LEASE_LENGTH}<br />
						<strong>Available By</strong>: ${AVAILABLE_BY}<br />
						${countBath} <strong>Bathrooms</strong><br />
						${countBed} <strong>Bedrooms</strong><br />
					</div>
					<div class="body-square no-pad body-square-no-pad" style="width:234px; margin-bottom:0px; display:none;">
						<div class="title-no-pad" style="width:204px;text-align: left; font-size: 15px;">Rooms</div>
						<div id="roomsContainer" style="width:224px; padding: 0px 5px 5px 5px;"></div>
					<div>
				</td>
				<td width="15" nowrap></td>
				<td width="234" class="body-square no-pad body-square-no-pad" style="width:234px;">
					<div class="title-no-pad" style="width:205px; font-size: 15px;">Amenities & Utilities</div>
					<div style="width:224px; padding: 0px 5px 5px 5px;">
						<table width="100%" cellpadding="0" cellspacing="0" id="utilitiesContainer">
						</table>
					</div>
				</td>
			</tr>
			<!--<tr><td colspan="3" class="spacer"></td></tr>
			<tr><td colspan="3" class="body-square no-pad body-square-no-pad" id="${SLIIDESHOWID}Container"></td></tr>-->
			<tr><td colspan="3" class="spacer"></td></tr>
			<tr><td colspan="3" align="right"><button id="backBot" class="yellowButtonUI">Go Back</button></td></tr>
		</table>
	</div>
</script>
<script id="imageTemplate" type="text/x-jquery-tmpl">
	<a href="<?php echo base_url (); ?>${LINK}"><img src="<?php echo base_url (); ?>${LINK}" alt="${CAPTION}"></a>
</script>
<script id="roomTemplate" type="text/x-jquery-tmpl">
	<span>
		${ROOM_NAME}: ${DIMENSION_1}x${DIMENSION_2}<br />
	</span>
</script>
<script id="utilityTemplate" type="text/x-jquery-tmpl">
	<tr><td valign="middle">
		<img src="<?php echo base_url (); ?>${LINK}" alt="" width="20" height="20">
		${TEXT}
	</td></tr>
</script>
<script id="searchUtilityTemplate" type="text/x-jquery-tmpl">
	<span title="${TEXT}" style="margin-left:5px;margin-top:5px;">
		<img src="<?php echo base_url (); ?>${LINK}" alt="" width="20" height="20">
	</span>
</script>