<div class="content border-all">
	<form action="<?php echo base_url('login/validate') ?>" method="post" id="login_form">
		<div class="error_message"><?php echo $this->session->flashdata('error_message');?></div>
		<div class="success_message"><?php echo $this->session->flashdata('success_message');?></div>
		<p>
			<label for="login">User: </label>
			<input type="text" name="login" id="login" placeholder="Login" />
		</p>
		<p>
			<label for="password">Password:</label>
			<input type="password" name="password" id="password" placeholder="Password" />
		</p>
		<p><button value="send" class="btn">Login</button></p>
	</form>
</div>