$(document).ready(function () {
	var password =
		{
			url : {
				changePassword: url + "login/password",
			}
			, template:
				{

					changePassword: $("#adminChangePasswordTemplate")
				}
		}
	;
	$('#adminChangePassword').click (function (event) {
		event.preventDefault ();
		dialog.create (
			template.toHTML (password.template.changePassword)
			, {
				title: 'Change Password'
				, autoOpen: true
				, height: 155
				, modal: true
				, create: function (event, ui) {
				}
				, buttons: {
					"Change Password": function () {

						dialog.submitForm ($(this), $('#adminChangePasswordForm'), password.url.changePassword);
					}
					, Cancel: dialog.close
				}
				, close: dialog.close
			}
		);
	});
});