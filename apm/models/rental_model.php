<?php
/*
{
   "ADDRESS" : {
      "CITY" : "Kingston",
      "HOUSE_NUMBER" : 5,
      "Lat" : 0.2,
      "Lng" : 0.2,
      "PROVINCE" : "Ontario",
      "STREET" : "Princess St"
   },
   "AVAILABLE_BY" : 2147483647,
   "BIG_ADVERT" : false,
   "DATE_ADDED" : 2147483647,
   "DESCRPITION" : "",

   "countBed" : 0,
   "countBath" : 0,

   "IMAGES" : [
      {
         "CAPTION" : "",
         "ID" : 0,
         "LINK" : "uploads/b57945deb81da9fc57a7b33022c2b46d.jpg"
      }
   ],
   "LEASE_LENGTH" : 4,
   "PRICE" : 1000,
   "ROOMS" : [
      {
         "DIMENSION_1" : 188,
         "DIMENSION_2" : 123,
         "ID" : 59,
         "ROOM_NAME" : "Master",
         "ROOM_TYPE" : "Bed"
      }
   ],
   "THUMBNAIL" : {
      "ID" : 2,
      "LINK" : "error"
   },
   "TYPE" : "Apartment",
   "UTILITIES" : {
      "GAS" : false,
      "GAS_AVAILABLE" : false,
      "HEAT" : false,
      "INTERNET" : false,
      "LAUNDRY" : false,
      "PARKING" : false,
      "PETS" : false,
      "POWER" : false,
      "SMOKING" : false,
      "WATER" : false
   },
   "VISIBLE" : false,
   "ID" : 3,
}
 */
/*
Rental_model
    [ID] => 2
    [DATE_ADDED] => 1348629145
    [TYPE] => Apartment
    [DESCRPITION] => aaa aaaaaaaaaaaaaaaaeao eao aoaoe aoea
    [AVAILABLE_BY] => 2147483647
    [LEASE_LENGTH] => 4
    [PRICE] => 1000
    [VISIBLE] => 1
    [countBed] => 1
    [countBath] => 1
    [BIG_ADVERT] => 1
    [address] => Object
            [HOUSE_NUMBER] => 599
            [STREET] => Victoria St
            [CITY] => Kingston
            [PROVINCE] => Ontario
            [Lat] => 44.2385718
            [Lng] => -76.50319200000001
    [util] => Object
            [PARKING] => 1
            [POWER] =>
            [HEAT] => 1
            [WATER] => 1
            [GAS] =>
            [GAS_AVAILABLE] =>
            [LAUNDRY] => 1
            [INTERNET] =>
            [SMOKING] =>
            [PETS] => 1
    [images] => Array
            [0] => Object
                    [ID] => 4
                    [LINK] => /apm/public/uploads/623a519ef184d856bfdcf6de21c8c769.jpg
                    [CAPTION] =>
            [1] => Object
                    [ID] => 5
                    [LINK] => /apm/public/uploads/1791c8891696593734835eca231bfc34.jpg
                    [CAPTION] => I love climbing
    [thumnail] => Object
            [ID] => 2
            [LINK] => /apm/public/uploads/0b9556026eb1b4ef0dabd8474316e88c.jpg
    [rooms] => Array
            [0] => Object
                    [ID] => 55
                    [ROOM_NAME] => aa
                    [ROOM_TYPE] => Bed
                    [DIMENSION_1] => 11
                    [DIMENSION_2] => 11
 */
class Rental_model extends CI_Model
{
	private $_table_rentals = "rentals";
	private $_table_images = "images";
	private $_table_rooms = "rooms";

	public $ID = 0;
	public $TYPE = "Apartment"; // set('Apartment','Commercial','Faculty','House','Parking','Room','Shared','Short term', 'Condo') NOT NULL,
	public $DESCRPITION = "";
	public $AVAILABLE_BY = 1389057475;
	public $DATE_ADDED = 1389057475;
	public $LEASE_LENGTH = 4; // # Months
	public $PRICE = 1000;
	public $VISIBLE = FALSE;
	public $BIG_ADVERT = FALSE;
	public $countBed = 0;
	public $countBath = 0;
	public $address = array (
		 'HOUSE_NUMBER' => 5
		 , 'STREET' => 'Princess St'
		 , 'CITY' => 'Kingston'
		 , 'PROVINCE' => 'Ontario'
		 , 'Lat' => 0.2
		 , 'Lng' => 0.2
	);
	public $util = array (
		'POWER' => FALSE
		, 'HEAT' => FALSE
		, 'WATER' => FALSE
	//	, 'GAS' => FALSE
		//, 'GAS_AVAILABLE' => FALSE
		, 'LAUNDRY' => FALSE
		, 'PARKING' => FALSE
		, 'INTERNET' => FALSE
		, 'SMOKING' => FALSE
		, 'PETS' => FALSE
	);
	public $images = array (
	);
	public $thumnail = array (
		'ID' => 1
		, 'LINK' => ""
	);
	public $rooms = array (
	);
	public function
		__construct			($_data = FALSE, $_thumnail = FALSE, $_images = FALSE, $_rooms = FALSE)
						{
							if ($_data === FALSE || $_thumnail === FALSE || $_images === FALSE || $_rooms === FALSE)
								parent::__construct ();
							else
							{
								$this->thumnail = array (
									'ID' => (int)$_thumnail[0]->ID
									, 'LINK' => $_thumnail[0]->LINK
								);
								$this->images = array ();
								foreach ($_images as $image)
								{
									$this->images[] = array (
										'ID' => (int)$image->ID
										, 'LINK' => $image->LINK
										, 'CAPTION' => $image->CAPTION
									);
								}
								$this->rooms = array ();
								foreach ($_rooms as $room)
								{
									//if ($room->ROOM_TYPE === 'Bedroom')
									//	$this->countBed ++;
									//if ($room->ROOM_TYPE === 'Bathroom')
									//	$this->countBath ++;

									$this->rooms[] = array (
											'ID' => (int)$room->ID
										  , 'ROOM_NAME' => $room->ROOM_NAME
										  , 'ROOM_TYPE' => $room->ROOM_TYPE
										  , 'DIMENSION_1' => $room->DIMENSION_1
										  , 'DIMENSION_2' => $room->DIMENSION_2
									);

								}
								$this->util = array (
									'PARKING' => $this->getBool ($_data->PARKING)
									, 'POWER' => $this->getBool ($_data->POWER)
									, 'HEAT' => $this->getBool ($_data->HEAT)
									, 'WATER' => $this->getBool ($_data->WATER)
									//, 'GAS' => $this->getBool ($_data->GAS)
									//, 'GAS_AVAILABLE' => $this->getBool ($_data->GAS_AVAILABLE)
									, 'LAUNDRY' => $this->getBool ($_data->LAUNDRY)
									, 'INTERNET' => $this->getBool ($_data->INTERNET)
									, 'SMOKING' => $this->getBool ($_data->SMOKING)
									, 'PETS' => $this->getBool ($_data->PETS)
								);
								$this->address = array (
									'HOUSE_NUMBER' => (int) $_data->HOUSE_NUMBER
									, 'STREET' => $_data->STREET
									, 'CITY' => $_data->CITY
									, 'PROVINCE' => $_data->PROVINCE
									, 'Lat' =>  $_data->Lat
									, 'Lng' =>  $_data->Lng
								);
								$this->VISIBLE = $this->getBool ($_data->VISIBLE);
								$this->BIG_ADVERT = $this->getBool ($_data->BIG_ADVERT);
								$this->ID = (int) $_data->ID;
								$this->TYPE = $_data->TYPE;
								$this->DESCRPITION = $_data->DESCRPITION;
								$this->AVAILABLE_BY = (int) $_data->AVAILABLE_BY;
								$this->DATE_ADDED = (int)  $_data->DATE_ADDED;
								$this->LEASE_LENGTH = (int)  $_data->LEASE_LENGTH;
								$this->PRICE = (int)  $_data->PRICE;
								$this->countBed = (int)  $_data->countBed;
								$this->countBath = (int)  $_data->countBath;
							}
						}
	public function
		get					($ID)
						{
							$this->db->where ('ID',	$ID);
							$this->selectAddRoomsCnt ();
							$query = $this->db->get ($this->_table_rentals);
							foreach ($query->result () as $rental)
							{
								$rooms = $this->db->get_where ($this->_table_rooms, array ('RENTAL_ID' => $rental->ID));
								$imgs = $this->db->get_where ($this->_table_images, array ('RENTAL_ID' => $rental->ID));
								$thumb = $this->db->get_where ($this->_table_images, array ('ID' => $rental->THUMBNAIL));
								$res = new Rental_model ($rental, $thumb->result (), $imgs->result (), $rooms->result ());
								return $res;
							}
							return null;
						}
	public function
		add					()
						{
							$this->db->insert ($this->_table_images, array (
								 'ID' => 0
								, 'LINK' => ''
								, 'CAPTION' => ''
								, 'RENTAL_ID' => 0
							));
							$this->thumnail['ID'] = $this->db->insert_id();

							$this->db->insert ($this->_table_rentals, $this->toArray ());
							$this->ID = $this->db->insert_id();

							return $this;
						}
	public function
		addImage			($data)
						{
							$this->db->insert ($this->_table_images, array (
								 'ID' => 0
								, 'RENTAL_ID' => $this->ID
								, 'LINK' => $data["LINK"]
								, 'CAPTION' => $data["CAPTION"]
							));
							$res = array (
								 'ID' => $this->db->insert_id()
								, 'LINK' => $data["LINK"]
								, 'CAPTION' => $data["CAPTION"]
							);
							$this->images[] = $res;
							return $res;
						}
	public function
		addRoom				($data)
						{
							$this->db->insert ($this->_table_rooms, array (
								 'ID' => 0
								, 'RENTAL_ID' => $this->ID
								, 'ROOM_NAME' => $data["ROOM_NAME"]
								, 'ROOM_TYPE' => $data["ROOM_TYPE"]
								, 'DIMENSION_1' => $data["DIMENSION_1"]
								, 'DIMENSION_2' => $data["DIMENSION_2"]
							));
							$res = array (
								 'ID' => $this->db->insert_id()
								, 'ROOM_NAME' => $data["ROOM_NAME"]
								, 'ROOM_TYPE' => $data["ROOM_TYPE"]
								, 'DIMENSION_1' => $data["DIMENSION_1"]
								, 'DIMENSION_2' => $data["DIMENSION_2"]
							);
							$this->rooms[] = $res;
							return $res;
						}
	public function
		update				()
						{
							$this->db->where('ID', $this->ID);
							$this->db->update ($this->_table_rentals, $this->toArray ());
							return $this;
						}
	public function
		updateThumbnail		($url)
						{
							$this->db->where('ID', $this->thumnail['ID']);
							$this->db->update ($this->_table_images, array ( 'LINK' => $url ));
							$this->thumnail['LINK'] = $url;
						}
	public function
		updateRoom			($data)
						{
							$this->db->where ('ID', $data['ID']);
							$this->db->update ($this->_table_rooms, array (
								'ROOM_NAME' => $data["ROOM_NAME"]
								, 'ROOM_TYPE' => $data["ROOM_TYPE"]
								, 'DIMENSION_1' => $data["DIMENSION_1"]
								, 'DIMENSION_2' => $data["DIMENSION_2"]
							));
							foreach ($this->rooms as $k => $v)
							{
								if ($this->rooms[$k]['ID'] == $data['ID'])
								{
									$this->rooms[$k]['ROOM_NAME'] = $data["ROOM_NAME"];
									$this->rooms[$k]['ROOM_TYPE'] = $data["ROOM_TYPE"];
									$this->rooms[$k]['DIMENSION_1'] = $data["DIMENSION_1"];
									$this->rooms[$k]['DIMENSION_2'] = $data["DIMENSION_2"];
									return $this->rooms[$k];
								}
							}
							return null;
						}
	public function
		remove				()
						{
							$this->db->where ('RENTAL_ID', $this->ID);
							$this->db->delete (array ($this->_table_images, $this->_table_rooms));
							$this->db->delete ($this->_table_images, array ('ID' => $this->thumnail['ID']));
							$this->db->delete ($this->_table_rentals, array ('ID' => $this->ID));
							$res = array ($this->thumnail ['LINK']);
							foreach ($this->images as $img)
								$res [] = $img ['LINK'];

							return $res;
						}
	public function
		removeRoom			($ID)
						{
							$this->db->delete ($this->_table_rooms, array ('ID' => $ID));
							foreach ($this->rooms as $k => $v)
							{
								if ($this->rooms[$k]['ID'] == $ID)
								{
									unset($this->rooms[$k]);
									return true;
								}
							}
							return false;
						}
	public function
		removeImage			($ID)
						{
							$this->db->delete ($this->_table_images, array ('ID' => $ID));
							$res = null;
							foreach ($this->images as $k => $v)
							{
								if ($this->images[$k]['ID'] == $ID)
								{
									$res = $this->images[$k];
									unset($this->images[$k]);
								}
							}
							return $res;
						}
	private function
		buildWhere			($filters)
						{
							// Booleans
							if (isset ($filters['VISIBLE']))		$this->db->where ('VISIBLE',		$this->boolToDB ($filters['VISIBLE']));
							if (isset ($filters['PARKING']))	$this->db->where ('PARKING',	$this->boolToDB ($filters['PARKING']));
							if (isset ($filters['POWER']))		$this->db->where ('POWER',		$this->boolToDB ($filters['POWER']));
							if (isset ($filters['HEAT']))		$this->db->where ('HEAT',		$this->boolToDB ($filters['HEAT']));
							if (isset ($filters['WATER']))		$this->db->where ('WATER',		$this->boolToDB ($filters['WATER']));
							if (isset ($filters['GAS']))		$this->db->where ('GAS',		$this->boolToDB ($filters['GAS']));
							if (isset ($filters['LAUNDRY']))	$this->db->where ('LAUNDRY',	$this->boolToDB ($filters['LAUNDRY']));
							if (isset ($filters['INTERNET']))	$this->db->where ('INTERNET',	$this->boolToDB ($filters['INTERNET']));
							if (isset ($filters['SMOKING']))	$this->db->where ('SMOKING',	$this->boolToDB ($filters['SMOKING']));
							if (isset ($filters['PETS']))		$this->db->where ('PETS',		$this->boolToDB ($filters['PETS']));
							if (isset ($filters['GAS_AVAILABLE']))
														$this->db->where ('GAS_AVAILABLE', $this->boolToDB ($filters['GAS_AVAILABLE']));

							// Ints
							if (isset ($filters['PRICE_G']))		$this->db->where ('PRICE >=',		$filters ['PRICE_G']);
							if (isset ($filters['PRICE_L']))		$this->db->where ('PRICE <=',		$filters ['PRICE_L']);
							if (isset ($filters['AVAILABLE_BY']))
														$this->db->where ('AVAILABLE_BY >', strtotime ($filters ['AVAILABLE_BY']));
							if (isset ($filters['countBed']))		$this->db->where ($this->getRoomCount ('Bedroom').' >=',		$filters ['countBed']);
							if (isset ($filters['countBath']))	$this->db->where ($this->getRoomCount ('Bathroom').' >=',		$filters ['countBath']);

							// Others
							if (isset ($filters['TYPE']))		$this->db->where ('TYPE',		$filters['TYPE']);

							//die ('aoeaoeaoeaoe aaoeao eao eao eaoe'.strtotime ($filters ['AVAILABLE_BY']));
						}
	private function
		getRoomCount			($col)
						{
							return '(SELECT COUNT(*) FROM '.$this->_table_rooms.' WHERE '.$this->_table_rooms.'.RENTAL_ID = '.$this->_table_rentals.'.ID AND '.$this->_table_rooms.'.ROOM_TYPE = "'.$col.'")';
						}
	public function
		selectAddRoomsCnt	()
						{
							$this->db->select ('*, '.$this->getRoomCount ('Bedroom'). ' AS countBed, '.$this->getRoomCount ('Bathroom'). ' AS countBath');
						}
	public function
		getRentalsLIstCount	($filters = FALSE)
						{
							if ($filters == FALSE)
								$this->db->where ('VISIBLE', 1);
							else
								$this->buildWhere ($filters);
							$c = 'COUNT(*)';
							$this->db->select ($c);
							$query = $this->db->get ($this->_table_rentals);
							foreach ($query->result () as $rental)
								return $rental->$c;
							return 0;
						}
	public function
		getRentalsLIst			($filters = FALSE, $toJSON = false)
						{
							if ($filters == FALSE)
								$this->db->where ('VISIBLE', 1);
							else
								$this->buildWhere ($filters);

							$this->selectAddRoomsCnt ();
							$query = $this->db->get ($this->_table_rentals, $this->getLimit ($filters), $this->getOffset ($filters));
							$res = array ();
							foreach ($query->result () as $rental)
							{
								$rooms = $this->db->get_where ($this->_table_rooms, array ('RENTAL_ID' => $rental->ID));
								$imgs = $this->db->get_where ($this->_table_images, array ('RENTAL_ID' => $rental->ID));
								$thumb = $this->db->get_where ($this->_table_images, array ('ID' => $rental->THUMBNAIL));
								if ($toJSON)
								{
									$r = new Rental_model ($rental, $thumb->result (), $imgs->result (), $rooms->result ());
									$res [] = $r->toJSON ();
								}
								else
									$res [] = new Rental_model ($rental, $thumb->result (), $imgs->result (), $rooms->result ());
							}
							return $res;
						}
	private function
		getLimit				($filters = FALSE)
						{
							return $filters == FALSE ? 10 : isset ($filters['limit']) ? $filters ['limit'] : 10;
						}
	private function
		getOffset			($filters = FALSE)
						{
							return $filters == FALSE ? 0 : isset ($filters['offset']) ? $filters ['offset'] : 0;
						}
	private function
		getBool				($value)
						{
							return $value == 1 || $value == "1" ? TRUE : FALSE;
						}
	private function
		boolToDB			($value)
						{
							return $value ? 1 : -1;
						}
	public function
		toArray				()
						{
							return array (
								 "THUMBNAIL" =>  $this->thumnail ['ID']
								, "TYPE" => $this->TYPE
								, "HOUSE_NUMBER" => $this->address['HOUSE_NUMBER']
								, "STREET" => $this->address['STREET']
								, "CITY" => $this->address['CITY']
								, "PROVINCE" => $this->address['PROVINCE']
								, "Lat" => $this->address['Lat']
								, "Lng" => $this->address['Lng']
								, "AVAILABLE_BY" => $this->AVAILABLE_BY
								, "LEASE_LENGTH" => $this->LEASE_LENGTH
								, "PRICE" => $this->PRICE
								, "DESCRPITION" => $this->DESCRPITION
								, "DATE_ADDED" => $this->DATE_ADDED

								, "VISIBLE" => $this->boolToDB ($this->VISIBLE)
								, "BIG_ADVERT" => $this->boolToDB ($this->BIG_ADVERT)

								, "PARKING" => $this->boolToDB ($this->util['PARKING'])
								, "POWER" => $this->boolToDB ($this->util['POWER'])
								, "HEAT" => $this->boolToDB ($this->util['HEAT'])
								, "WATER" => $this->boolToDB ($this->util['WATER'])
								//, "GAS" => $this->boolToDB ($this->util['GAS'])
								//, "GAS_AVAILABLE" =>$this->boolToDB ($this->util['GAS_AVAILABLE'])
								, "LAUNDRY" => $this->boolToDB ($this->util['LAUNDRY'])
								, "INTERNET" => $this->boolToDB ($this->util['INTERNET'])
								, "SMOKING" => $this->boolToDB ($this->util['SMOKING'])
								, "PETS" => $this->boolToDB ($this->util['PETS'])
							);
						}
	public function
		toJSON				()
						{
							return array (
								 "THUMBNAIL" => 0
								, "TYPE" => $this->TYPE
								, "ID" => $this->ID
								, "AVAILABLE_BY" => date ('Y-m-d', $this->AVAILABLE_BY)
								, "LEASE_LENGTH" => $this->LEASE_LENGTH
								, "PRICE" => $this->PRICE
								, "DESCRPITION" => $this->DESCRPITION
								, "DATE_ADDED" => $this->DATE_ADDED

								, "VISIBLE" => $this->VISIBLE
								, "BIG_ADVERT" => $this->BIG_ADVERT

								, 'countBed' => $this->countBed
								, 'countBath' => $this->countBath

								, 'UTILITIES' => $this->util
								, 'ADDRESS' => $this->address
								, 'THUMBNAIL' => $this->thumnail
								, 'IMAGES' => $this->images
								, 'ROOMS' => $this->rooms
							);
						}
	public function
		toString				()
						{
							return  json_encode ($this->toJSON ());
						}
}