<?php

class MY_Controller extends CI_Controller
{
	private $myLinks = array (
		array (
			'href'=>"/rentals"
			, 'id'=>"rentals"
			, 'class'=>"border-bottom"
			, 'title'=>"Rentals"
			, 'img'=>"images/avatar.png"
		)
		, array (
			'href'=>"#"
			, 'id'=>"page"
			, 'class'=>"border-bottom"
			, 'title'=>""
			, 'img'=>"images/avatar.png"
		)
	);
	private $myCSS = array (
		"css/pepper-grinder/jquery-ui-1.10.3.css"
		, "css/jquery.widgets.css"
	);
	private $myJS = array (
		"js/jquery.1.10.2.min.js"
		, "js/jquery-ui.1.10.3.min.js"
		, "js/jquery.ui.tmpl.js"
		, "js/jquery.liteuploader.js"
		, "js/galleria/galleria-1.3.3.min.js"
	);
	private $titleSufix = "Advantage Property Management";
	protected $rentals;
	protected $data = array ();

	public function
		__construct			()
						{
							parent::__construct ();
							if (ENVIRONMENT == 'development')
								$this->db->flush_cache ();

							$this->db->stop_cache ();
							$this->load->library ('user_agent'); // http://ellislab.com/codeigniter/user-guide/libraries/user_agent.html
							$this->load->model ('rental_model');
							$this->rentals = $this->rental_model;
							$this->load->model ('page_model');
							$this->pages = $this->page_model;
							$this->data = $this->input->post ();
						}
	protected function
		rental				()
						{
							$this->data ['ID'] = intval ($this->data ['ID']);
							if (!$this->data ['ID'])
							{
								$this->json (array (
									 'text' => 'Unable to find ID ('.$this->data ['ID'].'). The listing has been removed.'
									 , 'code' => 404
									 , 'isError' => true
								));
								return new Rental_model ();
							}
							else
							{
								$res = $this->rentals->get ($this->data ['ID']);
								if ($res === null)
								{
									$this->json (array (
										 'text' => 'Unable to find ID ('.$this->data ['ID'].'). The listing has been removed.'
										, 'code' => 404
										, 'isError' => true
									));
									return new Rental_model ();
								}
								else
									return $res;
							}
						}
	protected function
		page					($_id = FALSE)
						{
							if ($_id === FALSE)
							{
								$this->data ['id'] = intval ($this->data ['id']);
								if (!$this->data ['id'])
								{
									$this->json (array (
										 'text' => 'Unable to find ID ('.$this->data ['id'].'). The listing has been removed.'
										 , 'code' => 404
										 , 'isError' => true
									));
									return new Page_model ();
								}
								else
								{
									$res = $this->pages->getByID ($this->data ['id']);
									if ($res === null)
									{
										$this->json (array (
											 'text' => 'Unable to find ID ('.$this->data ['id'].'). The listing has been removed.'
											, 'code' => 404
											, 'isError' => true
										));
										return new Page_model ();
									}
									else
										return $res;
								}
							}
							else
							{
								$res = $this->pages->getByCleanTitle ($_id);
								if ($res === null)
								{
									$this->json (array (
										 'text' => 'Unable to find page title ('.$_id.'). The listing has been removed.'
										, 'code' => 404
										, 'isError' => true
									));
									return new Page_model ();
								}
								else
									return $res;
							}
						}

	private $jsonRan = false;
	protected function
		json					($data)
						{
							if ($this->jsonRan)
								return;
							$this->jsonRan = true;
							$this->output
								->set_content_type ('application/json')
								->set_output (json_encode ($data));
						}
	protected function
		render				($data, $page)
						{
							if (!isset($data['title']))
								$data['title'] = "";
							$data['page_title'] = $data['title'];

							foreach ($this->myLinks as $k => $d)
								if ($this->myLinks[$k]['id'] == $page)
								{
									$data['title'] .= $this->myLinks[$k]['title'] . " - " . $this->titleSufix;
									$data['bannerSRC'] = base_url ($this->myLinks [$k] ['img']);
									$this->myLinks[$k]['class'] .= '-active';
								}


							if ($data['title'] == "")
								$data['title'] = $this->titleSufix;

							$this->myCSS [] = "css/new-global.css" . (ENVIRONMENT == 'development' ? "?".time () : "");
							$this->myCSS [] = "css/gradients.css" . (ENVIRONMENT == 'development' ? "?".time () : "");
							$this->myCSS [] = "css/".$page.".css" . (ENVIRONMENT == 'development' ? "?".time () : "");

							$CSS = array ();
							foreach ($this->myCSS as $u)
								$CSS [] = base_url ($u);
							$JS = array ();
							foreach ($this->myJS as $u)
								$JS [] = base_url ($u);

							$this->load->view ('global.view.php', array(
								'title'		=> $data ['title'],
								'data'		=> $data,
								'is_mobile'	=> $this->agent->is_mobile (),
								'admin'		=> $this->user->validate_session (),
								'template'		=> $page,
								'myCSS'		=> $CSS,
								'myJS'		=> $JS,
								'navBar'		=> $this->myLinks
							));
						}
	protected function
		removeFile			($path)
						{
							if (strlen ($path) > 10)
								if (file_exists ($path))
									unlink ($path);
						}

	protected function
		uploadFiles			($crop = FALSE, $x = 150, $y = 150)
						{

							if (!isset($this->data ['liteUploader_id']))
								return array(
									'message' => 'missing file id'
									, 'url' => ''
								);
							$id = $this->data ['liteUploader_id'];
							if (!((isset($_FILES [$id] ['error'] [0])) && ($_FILES [$id] ['error'] [0] == 0)))
								return array(
									'message' => $_FILES [$id] ['error'][0]
									, 'url' => ''
								);

							$this->load->library ('image');
							$this->image->uploadTo = 'uploads/';
							$this->image->newName = md5 (time ());
							$this->image->returnType = 'array';
							$img = $this->image->upload ($_FILES [$id]);

							if ($img)
							{
								if ($crop)
								{
									$this->image->upscale = 'true';
									$this->image->newWidth = $x+1;
									$this->image->newHeight = $y+1;
									$i = $this->image->resize(); // resizes to 116px keeping aspect ratio
									// get new image height
									$imgWidth = $i['width'];
									// get new image width
									$imgHeight = $i['height'];
									if ($i)
									{
										// work out where to crop it
										$cropX = ($imgWidth > $x) ? (($imgWidth - $x) / 2) : 1;
										$cropY = ($imgHeight > $y) ? (($imgHeight - $y) / 2) : 1;
										$cropped = $this->image->crop ($x, $y, $cropX, $cropY);
										if ($cropped)
											return array(
												'message' => 'Success'
												, 'url' => $cropped ['path'].$cropped ['image']
											);
										else
											return array (
												'message' => 'Crop failed'
												, 'url' => ''
											);
									}
									else
										return array (
											'message' => 'Resize failed'
											, 'url' => ''
										);
								}
								else
									return array(
										'message' => 'Success'
										, 'url' => $img ['path'].$img ['image']
									);
							}
							return array(
								'message' => 'upload failed'
								, 'url' => ''
							);
						}
}
