<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route ['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route ['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route ['default_controller'] = 'rentals/index';
$route ['404_override'] = 'error/_404';

$route ['login'] = 'login/index';
$route ['login/'] = 'login/index';
$route ['login/bad'] = 'login/bad';
$route ['login/private_page'] = 'login/private_page';
$route ['login/validate'] = 'login/validate';
$route ['login/password'] = 'login/password';
$route ['login/logout'] = 'login/logout';

$route ['rentals/view/(:any)'] = 'rentals/view/$1';
$route ['rentals/find/(:any)'] = 'rentals/find/$1';
$route ['rentals/find/'] = 'rentals/index';
$route ['rentals/find'] = 'rentals/index';
$route ['rentals/'] = 'rentals/index';
$route ['rentals'] = 'rentals/index';
$route ['rentals/viewJSON'] = 'rentals/viewJSON';
$route ['rentals/rentalsJSON'] = 'rentals/rentalsJSON';
$route ['adminrentals/'] = 'adminrentals/index';
$route ['adminrentals'] = 'adminrentals/index';
$route ['adminrentals/glist'] = 'adminrentals/glist';
$route ['adminrentals/get'] = 'adminrentals/get';
$route ['adminrentals/create'] = 'adminrentals/create';
$route ['adminrentals/save'] = 'adminrentals/save';
$route ['adminrentals/matchType'] = 'adminrentals/matchType';
$route ['adminrentals/toggle'] = 'adminrentals/toggle';
$route ['adminrentals/delete'] = 'adminrentals/delete';
$route ['adminrentals/thumbnail'] = 'adminrentals/thumbnail';
$route ['adminrentals/addSlideshow'] = 'adminrentals/addSlideshow';
$route ['adminrentals/delSlideshow'] = 'adminrentals/delSlideshow';
$route ['adminrentals/updateRoom'] = 'adminrentals/updateRoom';
$route ['adminrentals/delRoom'] = 'adminrentals/delRoom';


$route ['page/save'] = 'adminpages/save';

$route ['(:any)'] = 'page/view/$1';
