-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 02, 2015 at 01:42 AM
-- Server version: 5.5.43-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `advantage_prop`
--

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE IF NOT EXISTS `images` (
  `ID` int(255) NOT NULL AUTO_INCREMENT,
  `RENTAL_ID` int(255) NOT NULL,
  `LINK` varchar(255) NOT NULL,
  `CAPTION` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`ID`, `RENTAL_ID`, `LINK`, `CAPTION`) VALUES
(1, 0, 'error', '');

-- --------------------------------------------------------

--
-- Table structure for table `index`
--

CREATE TABLE IF NOT EXISTS `index` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PAGE_CONTENT` text NOT NULL,
  `BANNER_CODE` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `index`
--

INSERT INTO `index` (`ID`, `PAGE_CONTENT`, `BANNER_CODE`) VALUES
(1, '<table border="0" cellspacing="20" cellpadding="20">\r\n<tbody>\r\n<tr>\r\n<td valign="top">\r\n<table border="0" cellspacing="5">\r\n<tbody>\r\n<tr>\r\n<td><img src="CATAGORIES/CONSTRUTION.png" alt="" width="80" height="80" /></td>\r\n<td style="width: 100%;">\r\n<h1>Looking To Rent?</h1>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</td>\r\n<td rowspan="2" align="center" valign="middle" nowrap="nowrap" width="25%">\r\n<h1><a href="../rentals/">Find Your Dream Home!</a></h1>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td valign="top">\r\n<p><strong><em>Advantage Property Management is a full service property management company&nbsp;</em><em>specializing in student rentals, multi-unit buildings, single family homes,&nbsp;</em><em>executive homes, and consulting in Kingston, Ontario.</em></strong></p>\r\n<p><strong>&nbsp;</strong></p>\r\n<p><strong><em>Advantage Property Management is a full service property management company&nbsp;</em><em>specializing in student rentals, multi-unit buildings, single family homes,&nbsp;</em><em>executive homes, and consulting in Kingston, Ontario.</em></strong></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<table style="width: 100%;" border="0" cellspacing="20" cellpadding="20">\r\n<tbody>\r\n<tr>\r\n<td style="width: 50%;" valign="top">\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<table border="0" cellspacing="5">\r\n<tbody>\r\n<tr>\r\n<td><a href="#Construction"><img src="CATAGORIES/CONSTRUTION.png" alt="" width="80" height="80" /></a></td>\r\n<td style="width: 100%;">\r\n<h1><a href="#Construction">Need a Handy Man?</a></h1>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td colspan="2">Since 1954, we''ve been kicking ass and taking names, leading in affordable, quality housing for people by incorporating 22,000 rental suites in a geographically diverse portfolio. Find a place to live in any of our 12 millemetres of space in Ontario!</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</td>\r\n<td valign="top">\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<table style="width: 100%;" border="0" cellspacing="5">\r\n<tbody>\r\n<tr>\r\n<td><a href="#Management"><img src="CATAGORIES/MANAGE.png" alt="" width="80" height="80" /></a></td>\r\n<td style="width: 100%;">\r\n<h1><a href="#Management">Need Management?</a></h1>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td colspan="2">\r\n<p>Since 1954, we''ve been kicking ass and taking names, leading in affordable, quality housing for people by incorporating 22,000 rental suites in a geographically diverse portfolio. Find a place to live in any of our 12 millemetres of space in Ontario!</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>', '<div></div>'),
(2, 'Call JOHN', ''),
(3, '<table border="0" cellspacing="20" cellpadding="20">\r\n<tbody>\r\n<tr>\r\n<td valign="top">\r\n<table border="0" cellspacing="5">\r\n<tbody>\r\n<tr>\r\n<td><a id="Management" href="#Management"><img src="../CATAGORIES/MANAGE.png" alt="" width="80" height="80" /></a></td>\r\n<td style="width: 100%;">\r\n<h1>Need Management?</h1>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</td>\r\n<td rowspan="2" align="center" valign="middle" width="25%" nowrap="nowrap">\r\n  <h1><a href="#">Request<br />a<br />Quote</a></h1>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td valign="top">\r\n  <p><strong><em>Advantage Property Management is a full service property management company&nbsp;</em><em>specializing in student rentals, multi-unit buildings, single family homes,&nbsp;</em><em>executive homes, and consulting in Kingston, Ontario.</em></strong></p>\r\n  <p><strong>&nbsp;</strong></p>\r\n  <p><strong><em>Advantage Property Management is a full service property management company&nbsp;</em><em>specializing in student rentals, multi-unit buildings, single family homes,&nbsp;</em><em>executive homes, and consulting in Kingston, Ontario.</em></strong></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>', ''),
(4, '<table border="0" cellspacing="20" cellpadding="20">\r\n<tbody>\r\n<tr>\r\n<td valign="top">\r\n<table border="0" cellspacing="5">\r\n<tbody>\r\n<tr>\r\n<td><a id="Construction" href="#Construction"><img src="../CATAGORIES/CONSTRUTION.png" alt="" width="80" height="80" /></a></td>\r\n<td style="width: 100%;">\r\n<h1>Need a Handy Man?</h1>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</td>\r\n<td rowspan="2" align="center" valign="middle" width="25%" nowrap="nowrap">\r\n  <h1><a href="#">Request<br />a<br />Quote</a></h1>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td valign="top">\r\n  <p><strong><em>Advantage Property Management is a full service property management company&nbsp;</em><em>specializing in student rentals, multi-unit buildings, single family homes,&nbsp;</em><em>executive homes, and consulting in Kingston, Ontario.</em></strong></p>\r\n  <p><strong>&nbsp;</strong></p>\r\n  <p><strong><em>Advantage Property Management is a full service property management company&nbsp;</em><em>specializing in student rentals, multi-unit buildings, single family homes,&nbsp;</em><em>executive homes, and consulting in Kingston, Ontario.</em></strong></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>', ''),
(5, 'Request a  viewing, call John', ''),
(6, 'Private Policy', ''),
(7, 'Permision to aquire', '');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE IF NOT EXISTS `permissions` (
  `id` mediumint(8) NOT NULL AUTO_INCREMENT,
  `name` varchar(12) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `description`) VALUES
(1, 'admin', 'System administrators.');

-- --------------------------------------------------------

--
-- Table structure for table `rentals`
--

CREATE TABLE IF NOT EXISTS `rentals` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `THUMBNAIL` int(255) NOT NULL,
  `TYPE` varchar(255) NOT NULL,
  `HOUSE_NUMBER` int(11) NOT NULL,
  `STREET` varchar(255) NOT NULL,
  `CITY` varchar(255) NOT NULL,
  `PROVINCE` varchar(255) NOT NULL,
  `Lat` varchar(255) NOT NULL DEFAULT '1',
  `Lng` varchar(255) NOT NULL DEFAULT '1',
  `AVAILABLE_BY` int(11) NOT NULL,
  `LEASE_LENGTH` int(1) NOT NULL,
  `PRICE` int(11) NOT NULL,
  `DESCRPITION` text NOT NULL,
  `DATE_ADDED` int(11) NOT NULL,
  `VISIBLE` tinyint(1) NOT NULL DEFAULT '0',
  `BIG_ADVERT` tinyint(1) NOT NULL DEFAULT '0',
  `PARKING` tinyint(1) NOT NULL DEFAULT '0',
  `POWER` tinyint(1) NOT NULL DEFAULT '0',
  `HEAT` tinyint(1) NOT NULL DEFAULT '0',
  `WATER` tinyint(1) NOT NULL DEFAULT '0',
  `GAS` tinyint(1) NOT NULL DEFAULT '0',
  `GAS_AVAILABLE` tinyint(1) NOT NULL DEFAULT '0',
  `LAUNDRY` tinyint(1) NOT NULL DEFAULT '0',
  `INTERNET` tinyint(1) NOT NULL DEFAULT '0',
  `SMOKING` tinyint(1) NOT NULL DEFAULT '0',
  `PETS` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE IF NOT EXISTS `rooms` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RENTAL_ID` int(4) NOT NULL,
  `ROOM_NAME` varchar(255) NOT NULL,
  `ROOM_TYPE` varchar(255) NOT NULL,
  `DIMENSION_1` smallint(6) NOT NULL,
  `DIMENSION_2` smallint(6) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=60 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` mediumint(8) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  `email` varchar(30) DEFAULT NULL,
  `login` varchar(18) NOT NULL,
  `password` varchar(60) NOT NULL,
  `last_login` date DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `login`, `password`, `last_login`, `active`) VALUES
(1, 'Administrator', 'admin@localhost', 'admin', '$2a$12$SR04o2/JNV5ZoVGZNgPiiezqM2f5D0eVDXsSDoWcfQqg/mST6O6Ye', '2000-01-01', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users_permissions`
--

CREATE TABLE IF NOT EXISTS `users_permissions` (
  `user_id` mediumint(8) NOT NULL,
  `permission_id` mediumint(8) NOT NULL,
  KEY `user_id` (`user_id`),
  KEY `permission_id` (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_permissions`
--

INSERT INTO `users_permissions` (`user_id`, `permission_id`) VALUES
(1, 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_permissions`
--
ALTER TABLE `users_permissions`
  ADD CONSTRAINT `users_permissions_ibfk_2` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `users_permissions_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
